# Servers

Tuuwi does not own physical servers. We are using external IT services to run our services on. The AGDSN server belongs to the organization.

## List

| Domain                                                           | Services                                                                                | Operating System                                                                                          | IPv4          | Provider  | Owned by tuuwi | Notes |
| ---------------------------------------------------------------- | --------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------------- | --------- | -------------- | ----- |
| tuuwi.de                                                         | Wordpress and Mailing services                                                          | SunOS hubbub 5.11 11.4.8.5.0 i86pc i386 i86pc Solaris                                                     | 81.169.145.78 | Strato AG | NO             |
| tuuwi-hub.de                                                     | HumHub, HumHub staging environment                                                      | Linux h2845994.stratoserver.net 4.15.0 #1 SMP Mon Mar 16 15:39:59 MSK 2020 x86_64 x86_64 x86_64 GNU/Linux | 81.169.204.58 | Strato AG | NO             |
| mumble.tuuwi.de, pad.tuuwi.de, scribble.tuuwi.de status.tuuwi.de | nginx (reverse proxy), etherpad lite, statusfy, mumble server, botamusique, scribble.rs | Linux vm-tuuwi 4.19.0-5-amd64 #1 SMP Debian 4.19.37-5 (2019-06-19) x86_64                                 | 141.30.30.139 | AGDSN     | YES            |
