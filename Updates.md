# Updates

## Automatic Security Updates

Security updates will be run daily on all servers. This is done via the "unattended-upgrades" package.

It was installed using this guide: https://wiki.ubuntuusers.de/Aktualisierungen/Konfiguration/#Automatische-Updates-ohne-Interaktion

To check whether the package installed security updates you can read the logfile at "/var/log/unattended-upgrades/unattended-upgrades.log".

Important: If the distribution or the repository changes, please edit the file "/etc/apt/apt.conf.d/50unattended-upgrades" accordingly.

## Manual Updates

The group decided to run manual updates at least every 2 weeks. Every member should check if there is any important activity on the servers before starting an upgrade of services. Keep in mind, that services could break, take your time for this task. You should also inform all members if there are downtimes to be expected.

Generally, all you need to do is updating the package via the "apt" package manager:

```shell
apt update && apt upgrade -y
```

Some packages require a manual restart using the "systemctl" or "service" package:

```shell
service $ServiceName restart
```

or

```shell
systemctl restart $ServiceName
```

You find more information about updating services in the [Services](./Services.md) file.

### Updating docker containers

Go to the service directory and change the tag of the desired container in the "docker-compose.yaml" or "docker-compose.yml" file.

After that you need to run the following commands to request an update and restart of the containers:

```shell
docker-compose down
docker-compose up -d
```

Remember to delete unused images once in a while. The docker image cache can grow large very quickly.

```shell
docker rmi $(grep -xvf <(docker ps -a --format '{{.Image}}') <(docker images | tail -n +2 | grep -v '<none>' | awk '{ print $1":"$2 }'))
```

### Only upgrade single apt package

If you do not want to upgrade all package, you can use this command to only upgrade a single package.

```bash
apt-get install --only-upgrade <packagename>
```


# Updating BBB

- Update the packages

```shell
apt update
apt upgrade
```

- Restart greenlight

```shell
cd greenlight/
docker-compose down
docker-compose up -d
```

- Run the script to set the customized media

```shell
cd ~/bbb-media/
sh customize.sh
```

- Make sure that https://bbb.tuuwi.de/monitoring/ is still reachable. If not, do the following steps to add monitoring to nginx reverse proxy.

Open the following file in your favorite text editor: /etc/nginx/sites-available/bigbluebutton

Add the following after: location = / { return 307 /b; }

```
# BigBlueButton monitoring
location /monitoring/ {
  proxy_pass         http://127.0.0.1:3001/;
  proxy_redirect     default;
  proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
  client_max_body_size       10m;
  client_body_buffer_size    128k;
  proxy_connect_timeout      90;
  proxy_send_timeout         90;
  proxy_read_timeout         90;
  proxy_buffer_size          4k;
  proxy_buffers              4 32k;
  proxy_busy_buffers_size    64k;
  proxy_temp_file_write_size 64k;
  include    fastcgi_params;
}
```

## Updating BBB docker containers

### Updating greenlight

- Stop the running greenlight instance

```shell
cd ~/greenlight/
docker-compose down
```

- Get the needed tag from docker hub at https://hub.docker.com/r/bigbluebutton/greenlight/tags and set it in the docker-compose file

```shell
vim docker-compose.yml
```

- Start the instance again in detached mode

```shell
docker-compose up -d
```

## Update bbb-monitoring

- Stop the running bbb-monitoring instance

```shell
cd bbb-monitoring/
docker-compose down
```

- Get the needed tag from docker hub and set it in the docker-compose file

```shell
vim docker-compose.yaml
```

- Start the instance again in detached mode

```shell
docker-compose up -d
```

- Make sure that all alerts are still working. You can see that they are working by checking out the CPU Utilization vs. Participants graph in the bbb.tuuwi.de dashboard. If it has a red line, everything is fine. If not, set the following alerts.

  - Click on "CPU Utilization vs. Participants" and then click on "Edit", click on "Alert". In the "Condition" fields, set the following: WHEN avg() OF query(A, 1m, now) IS ABOVE 75. In the "Notifications" fields set the following: Send to: agtechnik Message: CPU Utilization über 75%.
  - Click on "Memory Stats" and then click on "Edit", click on "Alert". In the "Condition" fields, set the following: WHEN avg() OF query(C, 1m, now) IS BELOW 5000000000. In the "Notifications" fields set the following: Send to: agtechnik Message: RAM available unter 5 GB.

## Restarting the BBB Server

Do not use "sudo reboot"! Only restart bilontu using the web UI. If the greenlight frontend is not working, do the following:

```shell
cd greenlight/
docker-compose down
docker-compose up -d
```