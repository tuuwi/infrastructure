# Infrastructure

A detailed overview of the infrastructure at tuuwi.

![agdsn](./diagram/agdsn.png)
![strato](./diagram/strato.png)

## Table of content

[Communication](./Communication.md)

[Mailing](./Mailing.md)

[Password-Management](./Password-Management.md)

[Servers](./Servers.md)

[Updates](./Updates.md)

[Services](./Services.md)

[Social-Media](./Social-Media.md)
