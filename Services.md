# Services

Tuuwi is strictly only trying to use free and open source software and to stay as privacy friendly as possible. We also try to keep the data under our control and try to save as little data as possible. Whenever possible, please use a pseudonym as we do not use any end to end encrypted services.

## Mailing

There are mailing addresses available with the domain tuuwi.de which can be used for groups within the organization. Mails are not under our control and also are not saved encrypted. We did not deploy the mailing server. Mails are not encrypted end to end, so please do not share sensitive information over mail.

## Wordpress

The Wordpress instance is running on https://tuuwi.de . We did not deploy this software. Therefore, the data is not completely under our control.

## HumHub

There is a humhub instance running on https://tuuwi-hub.de . Members get invited via mail to this instance. We did not deploy this software, so we do not have full control of the data. All data is saved unencrypted in a database on the webserver, so please do not share sensitive information.

## Owncast

Currently, we test livestreaming for the "AG Film" using [Owncast](https://github.com/owncast/owncast). This is running for on https://scribble.tuuwi.de for the testing period. It is planned for showing movies but could be used for other tuuwi events.

## Big Blue Button

> Please note that this server was shutdown on 27th November for financial reasons. We recommend you to move to bbb.agdsn.de. 
More infos can be found in the pad: https://pad.tuuwi.de/p/infos_bbb_abschaltung


## Mumble

There is a mumble instance at mumble.tuuwi.de . We installed mumble using the official documentation at https://wiki.mumble.info/wiki/Installing_Mumble . This instance is password protected and the password can be found on tuuwi-hub.de . Keep in mind that mumble is not end to end encrypted, so do not share sensitive information. There is also a botamusique music bot running on this mumble server. It can play audio from various sources. Keep in mind that there is a log of all the music you played. There is a manual on how to use mumble on https://tuuwi-hub.de .

## Etherpad-Lite

There is an Etherpad instance running on https://pad.tuuwi.de/ . We installed Etherpad-Lite using the official documentation at https://github.com/ether/etherpad-lite . Keep in mind that Etherpad is not end to end encrypted, so please do not share sensitive information.

## Scribble.rs

Scribble is a game where you have to draw a picture and other players have to guess the word. There is an instance running at https://scribble.tuuwi.de . We installed it using the the official documentation at https://github.com/scribble-rs/scribble.rs/ . There is a reverse proxy running in front of it as this game is completely unencrypted. So, if you play the game, please make sure that the setting "clients per IP" is set to the maximum value 24.

## Stausfy

The system status of tuuwi services can be seen at https://status.tuuwi.de. This is a static website. It will build itself once someone pushes to the [system-status Repository](https://codeberg.org/tuuwi/system-status).

![status](./diagram/status.png)
