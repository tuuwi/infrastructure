from diagrams import Cluster, Diagram
from diagrams.onprem.compute import Server
from diagrams.onprem.database import MySQL
from diagrams.onprem.network import Nginx
from diagrams.programming.language import NodeJS, Go
from diagrams.onprem.container import Docker


def build_agdsn():
    with Diagram("AGDSN", show=False, outformat="png"):
        with Cluster(""):
            vm = Server("tuuwi-agdsn")
            nginx = Nginx("Reverse Proxy")
            etherpad = NodeJS("Etherpad Lite")
            mysql = MySQL()
            # scribble = Go("Scribble.rs")
            owncast = Go("Owncast")
            vm >> nginx
            nginx >> etherpad
            etherpad >> mysql
            # nginx >> scribble
            nginx >> owncast
