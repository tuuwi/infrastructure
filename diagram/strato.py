from diagrams import Cluster, Diagram
from diagrams.onprem.compute import Server
from diagrams.onprem.database import MySQL
from diagrams.onprem.network import Apache
from diagrams.programming.language import NodeJS, Php

def build_strato():
    with Diagram("Strato", show=False, outformat="png"):
        with Cluster(""):
            tuuwi_hub_de = Server("tuuwi-hub.de")
            apache = Apache("Reverse Proxy")
            humhub = Php("HumHub")
            mysql = MySQL()
            etherpad = NodeJS("Etherpad Lite")
            tuuwi_de = Server("tuuwi.de")
            mail = Server("Managed E-Mail")
            wordpress = Php("Managed Wordpress")
            tuuwi_hub_de >> apache
            apache >> humhub >> mysql
            apache >> etherpad >> mysql
            tuuwi_de >> mail
            tuuwi_de >> wordpress