from diagrams import Cluster, Diagram
from diagrams.onprem.compute import Server
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.network import Nginx
from diagrams.saas.chat import Telegram
from diagrams.programming.language import Java
from diagrams.onprem.container import Docker

def build_hetzner():
    with Diagram("Hetzner", show=False, outformat="png"):
        with Cluster(""):
            vm = Server("bilontu")
            nginx = Nginx("Reverse Proxy")
            bbb = Java("Bigbluebutton")
            greenlight = Docker("greenlight")
            postgres = Docker("postgres:9.5")
            prometheus = Prometheus("Prometheus")
            grafana = Grafana("Grafana")
            telegram = Telegram("Alerting")
            node_exporter = Docker("node-exporter")
            bbb_exporter = Docker("bbb-exporter")
            vm >> nginx
            nginx >> greenlight >> postgres
            nginx >> grafana >> prometheus >> bbb_exporter >> bbb
            nginx >> bbb
            prometheus >> node_exporter
            grafana >> telegram