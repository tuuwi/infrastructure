from diagrams import Cluster, Diagram
from diagrams.onprem.compute import Server
from diagrams.onprem.database import MySQL
from diagrams.onprem.network import Nginx
from diagrams.programming.language import NodeJS, Go, Bash
from diagrams.onprem.container import Docker
from diagrams.onprem.client import Client
from diagrams.onprem.vcs import Git
from diagrams.saas.chat import Telegram


def build_status():
    with Diagram("Status", show=False, outformat="png"):
        with Cluster(""):
            local_repo = Client("local repo")
            remote_repo = Git("codeberg.org")
            # telegram = Telegram("tuuwi group")
            with Cluster("AGDSN"):
                nginx = Nginx("Reverse Proxy")
                webhook = Go("webhook")
                build_script = Bash("build script")
            local_repo >> remote_repo
            remote_repo >> nginx
            nginx >> webhook
            webhook >> build_script
            # build_script >> telegram
