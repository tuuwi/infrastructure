# Infrastructure Diagrams

## Usage with VSCode

Follow the tutorial here: https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-1/

## Installation

```shell
poetry install
```

## Build Diagrams

```shell
python build_diagrams.py
```